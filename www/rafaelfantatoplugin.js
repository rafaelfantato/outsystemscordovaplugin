
var exec = require('cordova/exec');
module.exports = {
    rf_concatenate: function(value1, value2,successCallback, errorCallback) {
        
		var options = {};
	  	options.value1 = value1;
	  	options.value2 = value2;
		exec(successCallback, errorCallback, 'RafaelfantatoPlugin', 'concatenate', [options]);
    }
}

/*
// The function that passes work along to native shells
RafaelfantatoPlugin.prototype.concatenate = function(value1, value2, successCallback, errorCallback) {
  var options = {};
  options.value1 = value1;
  options.value2 = value2;
  cordova.exec(successCallback, errorCallback, 'RafaelfantatoPlugin', 'concatenate', [options]);
}

// Installation constructor that binds ToastyPlugin to window
RafaelfantatoPlugin.install = function() {
  if (!window.plugins) {
    window.plugins = {};
  }
  window.plugins.rafaelfantatoPlugin = new RafaelfantatoPlugin();
  return window.plugins.rafaelfantatoPlugin;
};
cordova.addConstructor(RafaelfantatoPlugin.install);*/