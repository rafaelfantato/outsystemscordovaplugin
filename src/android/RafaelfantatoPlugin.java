package com.rafaelfantato.cordova.plugin;
// Cordova-required packages
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
public class RafaelfantatoPlugin extends CordovaPlugin {

  @Override
  public boolean execute(String action, JSONArray args,
    final CallbackContext callbackContext) {
      // Verify that the user sent a 'concatenate' action
      if (!action.equals("concatenate")) {
        callbackContext.error("\"" + action + "\" is not a recognized action.");
        return false;
      }
      String value1;
      String value2;
      String resultValue;
      try {
        JSONObject options = args.getJSONObject(0);
        value1 = options.getString("value1");
        value2 = options.getString("value2");
      } catch (JSONException e) {
        callbackContext.error("Error encountered: " + e.getMessage());
        return false;
      }
      // Do the magic

      // Send a positive result to the callbackContext
      
      resultValue = value1+" "+value2;

      callbackContext.success(resultValue);
      return true;
  }
}